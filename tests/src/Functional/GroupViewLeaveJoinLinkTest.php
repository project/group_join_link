<?php

namespace Drupal\Tests\group_join_link\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\group\Functional\GroupBrowserTestBase;

/**
 * Tests for views group leave / join links.
 *
 * @group group
 */
class GroupViewLeaveJoinLinkTest extends GroupBrowserTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'group',
    'group_test_config',
    'group_join_link',
    'views',
    'test_group_join_link',
  ];

  /**
   * The group we will use to test methods on.
   *
   * @var \Drupal\group\Entity\Group
   */
  protected $group;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->group = $this->createGroup(['type' => 'default']);
  }

  /**
   * Test link for outsider without join permission.
   */
  public function testLinkForOuitsiderWithoutJoinPermissions() {
    $account = $this->createUser();
    $this->drupalLogin($account);

    $this->group->getGroupType()->getOutsiderRole()->revokePermission('join group')->save();

    $this->drupalGet('/groups-list');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->linkNotExists('Join');
  }

  /**
   * Test link for outsider with join permission.
   */
  public function testLinkForOuitsiderWithJoinPermissions() {
    $account = $this->createUser();
    $this->drupalLogin($account);

    $this->group->getGroupType()->getOutsiderRole()->grantPermission('join group')->save();

    $this->drupalGet('/groups-list');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->linkExists('Join');
  }

  /**
   * Test link for member without leave permission.
   */
  public function testLinkForMemberWithoutLeavePermissions() {
    $account = $this->createUser();
    $this->drupalLogin($account);

    $this->group->addMember($account);

    $this->group->getGroupType()->getMemberRole()->revokePermission('leave group')->save();

    $this->drupalGet('/groups-list');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->linkNotExists('Leave');
  }

  /**
   * Test link for member with leave permission.
   */
  public function testLinkForMemberWithLeavePermissions() {
    $account = $this->createUser();
    $this->drupalLogin($account);

    $this->group->getGroupType()->getMemberRole()->grantPermission('leave group')->save();

    $this->group->addMember($account);

    $this->drupalGet('/groups-list');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->linkExists('Leave');
  }

}

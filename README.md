# INTRODUCTION

Group views join link - This module extends [group](https://www.drupal.org/project/group) module. It allows to add Join / Leave link for the given group in your views.

# REQUIREMENTS

This module requires the following modules:

* [Group](https://www.drupal.org/project/group)
* [Views](part of the Drupal core)

# INSTALLATION

You can install this module using standard user interface or using cli command

```
drush en group_join_link
```


# Maintainers

[Nikolay Lobachev](https://www.drupal.org/u/lobsterr)

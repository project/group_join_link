<?php

namespace Drupal\group_join_link\Plugin\views\field;

use Drupal\Core\Link;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides join / leave links for a group.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("group_join_leave_link")
 */
final class JoinLeaveLink extends FieldPluginBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * RequestMembership constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountInterface $current_user,
    CurrentPathStack $current_path
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->currentPath = $current_path;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('path.current')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Intentionally override query to do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\group\Entity\Group $group */
    $group = $values->_entity;
    if (!($group instanceof GroupInterface) && !empty($values->_relationship_entities['gid'])) {
      $group = $values->_relationship_entities['gid'];
    }

    $build = NULL;
    if (empty($group)) {
      return $build;
    }

    $link_params = [
      'group' => $group->id(),
      'destination' => $this->currentPath->getPath(),
    ];
    if (empty($group->getMember($this->currentUser))) {
      if ($group->hasPermission('join group', $this->currentUser)) {
        $build = Link::createFromRoute($this->t('Join group'), 'entity.group.join', $link_params)->toString();
      }
    }
    elseif ($group->hasPermission('leave group', $this->currentUser)) {
      $build = Link::createFromRoute($this->t('Leave group'), 'entity.group.leave', $link_params)->toString();
    }

    return $build;
  }

}
